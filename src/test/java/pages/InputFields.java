package pages;

import com.codeborne.selenide.SelenideElement;

import java.io.File;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;

public class InputFields {

    SelenideElement firstName = $("#firstName"),
            lastName = $("#lastName"),
            userEmail = $("#userEmail"),
            userNumber = $("#userNumber"),
            radioButton = $(byText("Male")),
            dateOfBirth = $("#dateOfBirthInput"),
            month = $(".react-datepicker__month-select"),
            year = $(".react-datepicker__year-select"),
            day = $(".react-datepicker__day--017"),
            subjectsInput = $("#subjectsInput"),
            checkBoxSports = $(byText("Sports")),
            checkBoxMusic = $(byText("Music")),
            uploadFile = $("#uploadPicture"),
            currentAddress = $("#currentAddress"),
            selectState = $(byText("Select State")),
            chooseState = $(byText("NCR")),
            selectCity = $(byText("Select City")),
            chooseCity = $(byText("Delhi")),
            clickSubmit = $(byText("Submit"));

    public InputFields openPracticeForm() {
        open("https://demoqa.com/automation-practice-form");
        return this;
    }

    public InputFields setFirstName(String firstNameValue) {
        firstName.val(firstNameValue);
        return this;
    }

    public InputFields setLastName(String lastNameValue) {
        lastName.val(lastNameValue);
        return this;
    }

    public InputFields setUserEmail(String userEmailValue) {
        userEmail.val(userEmailValue);
        return this;
    }

    public InputFields setUserNumber(String userNumberValue) {
        userNumber.val(userNumberValue);
        return this;
    }

    public InputFields clickRadioButtonGender(String gender) {
        radioButton.click();
        return this;
    }

    public InputFields clickOnDateOfBirth() {
        dateOfBirth.click();
        return this;
    }

    public InputFields selectDay(String birthDay) {
        day.click();
        return this;
    }

    public InputFields selectMonth(String birthMonth) {
        month.selectOption(birthMonth);
        return this;
    }

    public InputFields selectYear(String birthYear) {
        year.selectOption(birthYear);
        return this;
    }


    public InputFields setSubject(String subject) {
        subjectsInput.val(subject).pressEnter();
        return this;
    }

    public InputFields clickCheckBoxMusic(String hobby) {
        checkBoxMusic.click();
        return this;
    }

    public InputFields clickCheckBoxSports(String hobby) {
        checkBoxSports.click();
        return this;
    }

    public InputFields uploadFile(String fileSource) {
        uploadFile.uploadFile(new File(fileSource));
        return this;
    }

    public InputFields setCurrentAddress(String address) {
        currentAddress.val(address);
        return this;
    }

    public InputFields selectState() {
        selectState.scrollTo().click();
        return this;
    }

    public InputFields chooseState(String state) {
        chooseState.click();
        return this;
    }

    public InputFields selectCity() {
        selectCity.click();
        return this;
    }

    public InputFields chooseCity(String city) {
        chooseCity.click();
        return this;
    }

    public InputFields clickSubmit() {
        clickSubmit.click();
        return this;
    }
}

